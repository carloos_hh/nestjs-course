import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UV_FS_O_FILEMAP } from 'constants';
import { User } from 'src/auth/user.entity';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksDto } from './dto/get-tasks.dto';
import { TaskStatus } from './task-status.enum';
import { Task } from './task.entity';
import { TaskRepository } from './task.repository';

@Injectable()
export class TasksService {
    constructor(
        @InjectRepository(TaskRepository)
        private readonly taskRepository: TaskRepository
    ) {}

   getTasks(
       filterDto: GetTasksDto,
        user: User
    ): Promise<Task[]> {
      return  this.taskRepository.getTasks(filterDto, user);
   }

    async getTaskById(
        id: number,
        user: User,
        ): Promise<Task>{
        const taskExists = await this.taskRepository.findOne( {where: {id, userId: user.id}} );

        if(!taskExists){
            throw new NotFoundException(`Task with id: ${id} not found.`);
        }

        return taskExists;
    }


    async createTask(
        createTaskDto: CreateTaskDto,
        user: User,
        ): Promise<Task> {
        return this.taskRepository.createTask(createTaskDto, user);
    }

    async deleteTask(id: number, user: User,): Promise<void> {
         const result = await this.taskRepository.delete({id, userId: user.id});
          if(result.affected === 0){
            throw new NotFoundException(`Task with id: ${id} not found.`);
          }
    }

    async updateTask(id: number, status: TaskStatus, user: User): Promise<Task> {
        const taskExists = await this.getTaskById(id, user);
        taskExists.status = status;
        await taskExists.save()

        return taskExists;
    }

}
